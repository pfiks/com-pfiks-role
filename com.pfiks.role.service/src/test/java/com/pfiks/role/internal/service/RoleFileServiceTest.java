/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.internal.service;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.InputStream;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.model.RoleModel;

public class RoleFileServiceTest {

	private static final String ROLE_NAME = "Test role name";

	private static final int ROLE_TYPE = 1;

	@InjectMocks
	private RoleFileService roleFileService;

	@Test(expected = RoleConfigurationException.class)
	public void getRoleModelFromInputStream_WhenException_ThenThrowsRoleConfiguratorException() throws Exception {
		roleFileService.getRoleModelFromInputStream(null);
	}

	@Test @Ignore
	public void getRoleModelFromInputStream_WhenNoError_ThenReturnsTheRoleModelFromTheFile() throws Exception {
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("import-test/validRoleNoPermissions.xml");

		RoleModel result = roleFileService.getRoleModelFromInputStream(inputStream);

		verifyRole(result);
	}

	@Before
	public void setUp() {
		initMocks(this);
	}

	private void verifyRole(RoleModel result) {
		assertThat(result.getName(), is(ROLE_NAME));
		assertThat(result.getType(), is(ROLE_TYPE));
	}

}
