/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.internal.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.pfiks.common.locale.LocaleMapUtil;
import com.pfiks.common.servicecontext.ServiceContextHelper;
import com.pfiks.role.model.ResourcePermissionModel;
import com.pfiks.role.model.RoleModel;

public class RoleConfiguratorUtilTest {

	private static final long COMPANY_ID = 11L;

	private static final long GROUP_ID = 2L;

	private static final String RESOURCE_ONE_FIRST_ACTION_ID = "myFirstActionId";

	private static final String RESOURCE_ONE_NAME = "myResourcePermissionNameOne";

	private static final String RESOURCE_ONE_PRIM_KEY = String.valueOf(COMPANY_ID);

	private static final int RESOURCE_ONE_SCOPE = 10;

	private static final String RESOURCE_ONE_SECOND_ACTION_ID = "mySecondActionId";

	private static final String RESOURCE_TWO_FIRST_ACTION_ID = "myThirdActionId";

	private static final String RESOURCE_TWO_NAME = "myResourcePermissionNameTwo";

	private static final String RESOURCE_TWO_PRIM_KEY = "myResourcePrimKey";

	private static final int RESOURCE_TWO_SCOPE = 20;

	private static final long ROLE_ID = 4L;

	private static final String ROLE_NAME = "Test role name";

	private static final String ROLE_TITLE = "Test role title";

	private static final int ROLE_TYPE = 1;

	private static final long USER_ID = 3L;

	private LinkedList<String> actionIdsResourcePermissionOne;

	private LinkedList<String> actionIdsResourcePermissionTwo;

	@Mock
	private Group mockGroup;

	@Mock
	private GroupLocalService mockGroupLocalService;

	@Mock
	private LocaleMapUtil mockLocaleMapUtil;

	@Mock
	private ResourcePermissionLocalService mockResourcePermissionLocalService;

	@Mock
	private ResourcePermissionModel mockResourcePermissionModelOne;

	@Mock
	private ResourcePermissionModel mockResourcePermissionModelTwo;

	@Mock
	private Role mockRole;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Mock
	private RoleModel mockRoleModel;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private ServiceContextHelper mockServiceContextHelper;

	@Mock
	private Map<Locale, String> mockTitleMap;

	@Mock
	private UserLocalService mockUserLocalService;

	private List<ResourcePermissionModel> resourcePermissionsList;

	private RoleConfiguratorUtil roleConfiguratorUtil;

	@Test
	public void configurePermissions_WhenNoResourcePermissionsAreSpecified_ThenNoActionIsPerformed() throws Exception {
		when(mockRoleModel.getResourcePermissions()).thenReturn(resourcePermissionsList);

		roleConfiguratorUtil.configurePermissions(mockRole, mockRoleModel, false);

		verifyNoInteractions(mockResourcePermissionLocalService);
	}

	@Test(expected = PortalException.class)
	public void configurePermissions_WhenResourcePermissionIsConfiguredAsResourcePermission_AndRoleOneThrowsException_AndSkipResourceIsFalse_ThenThrowsException() throws Exception {
		mockPermissionsDetails();
		resourcePermissionsList.add(mockResourcePermissionModelOne);
		resourcePermissionsList.add(mockResourcePermissionModelTwo);

		when(mockRoleModel.getResourcePermissions()).thenReturn(resourcePermissionsList);
		when(mockRole.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockRole.getRoleId()).thenReturn(ROLE_ID);

		doThrow(PortalException.class).when(mockResourcePermissionLocalService).setResourcePermissions(COMPANY_ID, RESOURCE_ONE_NAME, RESOURCE_ONE_SCOPE, String.valueOf(COMPANY_ID), ROLE_ID,
				actionIdsResourcePermissionOne.toArray(new String[actionIdsResourcePermissionOne.size()]));

		roleConfiguratorUtil.configurePermissions(mockRole, mockRoleModel, false);

		verify(mockResourcePermissionLocalService, times(1)).setResourcePermissions(COMPANY_ID, RESOURCE_TWO_NAME, RESOURCE_TWO_SCOPE, RESOURCE_TWO_PRIM_KEY, ROLE_ID,
				actionIdsResourcePermissionTwo.toArray(new String[actionIdsResourcePermissionTwo.size()]));
	}

	@Test
	public void configurePermissions_WhenResourcePermissionIsConfiguredAsResourcePermission_AndRoleOneThrowsException_AndSkipResourceIsTrue_ThenSetsResourceTwo() throws Exception {
		mockPermissionsDetails();
		resourcePermissionsList.add(mockResourcePermissionModelOne);
		resourcePermissionsList.add(mockResourcePermissionModelTwo);

		when(mockRoleModel.getResourcePermissions()).thenReturn(resourcePermissionsList);
		when(mockRole.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockRole.getRoleId()).thenReturn(ROLE_ID);

		doThrow(PortalException.class).when(mockResourcePermissionLocalService).setResourcePermissions(COMPANY_ID, RESOURCE_ONE_NAME, RESOURCE_ONE_SCOPE, String.valueOf(COMPANY_ID), ROLE_ID,
				actionIdsResourcePermissionOne.toArray(new String[actionIdsResourcePermissionOne.size()]));

		roleConfiguratorUtil.configurePermissions(mockRole, mockRoleModel, true);

		verify(mockResourcePermissionLocalService, times(1)).setResourcePermissions(COMPANY_ID, RESOURCE_TWO_NAME, RESOURCE_TWO_SCOPE, RESOURCE_TWO_PRIM_KEY, ROLE_ID,
				actionIdsResourcePermissionTwo.toArray(new String[actionIdsResourcePermissionTwo.size()]));
	}

	@Test
	public void configurePermissions_WhenResourcePermissionsAreSpecifiedAndAreNotSupported_ThenPermissionsAreConfiguredAsResourcePermissionsForTheRole() throws Exception {
		mockPermissionsDetails();
		resourcePermissionsList.add(mockResourcePermissionModelOne);
		resourcePermissionsList.add(mockResourcePermissionModelTwo);

		when(mockRoleModel.getResourcePermissions()).thenReturn(resourcePermissionsList);
		when(mockRole.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockRole.getRoleId()).thenReturn(ROLE_ID);

		roleConfiguratorUtil.configurePermissions(mockRole, mockRoleModel, false);

		verify(mockResourcePermissionLocalService, times(1)).setResourcePermissions(COMPANY_ID, RESOURCE_ONE_NAME, RESOURCE_ONE_SCOPE, String.valueOf(COMPANY_ID), ROLE_ID,
				actionIdsResourcePermissionOne.toArray(new String[actionIdsResourcePermissionOne.size()]));

		verify(mockResourcePermissionLocalService, times(1)).setResourcePermissions(COMPANY_ID, RESOURCE_TWO_NAME, RESOURCE_TWO_SCOPE, RESOURCE_TWO_PRIM_KEY, ROLE_ID,
				actionIdsResourcePermissionTwo.toArray(new String[actionIdsResourcePermissionTwo.size()]));
	}

	@Test
	public void createRole_WhenNoError_ThenReturnsTheCreatedRole() throws Exception {
		when(mockRoleModel.getName()).thenReturn(ROLE_NAME);
		when(mockRoleModel.getTitle()).thenReturn(ROLE_TITLE);
		when(mockRoleModel.getType()).thenReturn(ROLE_TYPE);
		when(mockLocaleMapUtil.getLocaleMapForAvailableLocales(ROLE_TITLE)).thenReturn(mockTitleMap);
		when(mockGroupLocalService.getCompanyGroup(COMPANY_ID)).thenReturn(mockGroup);
		when(mockGroup.getGroupId()).thenReturn(GROUP_ID);
		when(mockUserLocalService.getGuestUserId(COMPANY_ID)).thenReturn(USER_ID);
		when(mockServiceContextHelper.getServiceContext(COMPANY_ID, GROUP_ID, USER_ID)).thenReturn(mockServiceContext);
		when(mockRoleLocalService.addRole(USER_ID, Role.class.getName(), 0, ROLE_NAME, mockTitleMap, mockTitleMap, ROLE_TYPE, "", mockServiceContext)).thenReturn(mockRole);

		Role result = roleConfiguratorUtil.createRole(COMPANY_ID, mockRoleModel);

		assertThat(result, sameInstance(mockRole));
	}

	@Before
	public void setUp() throws Exception {
		initMocks(this);

		roleConfiguratorUtil = new RoleConfiguratorUtil();
		roleConfiguratorUtil.setGroupLocalService(mockGroupLocalService);
		roleConfiguratorUtil.setLocaleMapUtil(mockLocaleMapUtil);
		roleConfiguratorUtil.setResourcePermissionLocalService(mockResourcePermissionLocalService);
		roleConfiguratorUtil.setRoleLocalService(mockRoleLocalService);
		roleConfiguratorUtil.setServiceContextHelper(mockServiceContextHelper);
		roleConfiguratorUtil.setUserLocalService(mockUserLocalService);

		resourcePermissionsList = new LinkedList<>();
		actionIdsResourcePermissionOne = new LinkedList<>();
		actionIdsResourcePermissionTwo = new LinkedList<>();
	}

	private void mockPermissionsDetails() {
		actionIdsResourcePermissionOne.add(RESOURCE_ONE_FIRST_ACTION_ID);
		actionIdsResourcePermissionOne.add(RESOURCE_ONE_SECOND_ACTION_ID);

		actionIdsResourcePermissionTwo.add(RESOURCE_TWO_FIRST_ACTION_ID);

		mockResourcePermissionDetails(mockResourcePermissionModelOne, RESOURCE_ONE_NAME, RESOURCE_ONE_PRIM_KEY, true, actionIdsResourcePermissionOne, RESOURCE_ONE_SCOPE);
		mockResourcePermissionDetails(mockResourcePermissionModelTwo, RESOURCE_TWO_NAME, RESOURCE_TWO_PRIM_KEY, false, actionIdsResourcePermissionTwo, RESOURCE_TWO_SCOPE);
	}

	private void mockResourcePermissionDetails(ResourcePermissionModel resourcePermission, String name, String primaryKey, boolean companyPrimKey, List<String> actionIds, int scope) {
		when(resourcePermission.getName()).thenReturn(name);
		when(resourcePermission.isPrimKeyCompany()).thenReturn(companyPrimKey);
		when(resourcePermission.getPrimKey()).thenReturn(primaryKey);
		when(resourcePermission.getScope()).thenReturn(scope);
		when(resourcePermission.getActionIds()).thenReturn(actionIds);
	}

}
