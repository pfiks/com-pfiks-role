/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.internal.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.ResourceAction;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.ResourcePermission;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ResourceActionLocalService;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;
import com.liferay.portal.kernel.util.GetterUtil;
import com.pfiks.role.exception.RolePermissionException;

public class RolePermissionUtilTest {

	private RolePermissionUtil rolePermissionUtil;

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private ResourceActionLocalService mockResourceActionLocalService;

	@Mock
	private ResourcePermissionLocalService mockResourcePermissionLocalService;

	@Mock
	private ResourceAction mockResourceAction;

	@Mock
	private ResourcePermission mockResourcePermission;

	@Before
	public void setUp() {
		initMocks(this);

		rolePermissionUtil = new RolePermissionUtil();

		rolePermissionUtil.setCounterLocalService(mockCounterLocalService);
		rolePermissionUtil.setResourceActionLocalService(mockResourceActionLocalService);
		rolePermissionUtil.setResourcePermissionLocalService(mockResourcePermissionLocalService);
	}

	@Test(expected = RolePermissionException.class)
	public void addActionIdToResourcePermission_WhenException_ThenThrowsRolePermissionException() throws Exception {
		rolePermissionUtil.addActionIdToResourcePermission(ActionKeys.UPDATE, null);
	}

	@Test
	public void addActionIdToResourcePermission_WhenActionIdIsNotView_ThenConfiguresAndUpdatesTheResourceActionWithoutViewActionId() throws Exception {
		rolePermissionUtil.addActionIdToResourcePermission(ActionKeys.UPDATE, mockResourcePermission);

		InOrder inOrder = inOrder(mockResourcePermission, mockResourcePermissionLocalService);
		inOrder.verify(mockResourcePermission, times(1)).addResourceAction(ActionKeys.UPDATE);
		inOrder.verify(mockResourcePermissionLocalService, times(1)).updateResourcePermission(mockResourcePermission);
		verify(mockResourcePermission, never()).setViewActionId(anyBoolean());
	}

	@Test
	public void addActionIdToResourcePermission_WhenActionIdIsView_ThenConfiguresAndUpdatesTheResourceActionWithViewActionIdTrue() throws Exception {
		rolePermissionUtil.addActionIdToResourcePermission(ActionKeys.VIEW, mockResourcePermission);

		InOrder inOrder = inOrder(mockResourcePermission, mockResourcePermissionLocalService);
		inOrder.verify(mockResourcePermission, times(1)).setViewActionId(true);
		inOrder.verify(mockResourcePermission, times(1)).addResourceAction(ActionKeys.VIEW);
		inOrder.verify(mockResourcePermissionLocalService, times(1)).updateResourcePermission(mockResourcePermission);
	}

	@Test(expected = RolePermissionException.class)
	public void addResourcePermission_WhenException_ThenThrowsRolePermissionException() throws Exception {
		rolePermissionUtil.addResourcePermission(1l, "RESOURCE_NAME", "RESOURCE_PRIM_KEY", ActionKeys.UPDATE, 2l);
	}

	@Test
	public void addResourcePermission_WhenActionIdIsView_ThenCreatesAndConfiguresTheResourcePermissionWithViewActionIdTrue() throws Exception {
		long nextId = 123L;
		Long bitwiseValue = 456l;
		long companyId = 789L;
		long roleId = 22L;
		String resourceName = "myResourcePermissionName";
		String primKey = "654987";
		when(mockResourceActionLocalService.getResourceAction(resourceName, ActionKeys.VIEW)).thenReturn(mockResourceAction);
		when(mockResourceAction.getBitwiseValue()).thenReturn(bitwiseValue);
		when(mockCounterLocalService.increment(ResourcePermission.class.getName(), 1)).thenReturn(nextId);
		when(mockResourcePermissionLocalService.createResourcePermission(nextId)).thenReturn(mockResourcePermission);

		rolePermissionUtil.addResourcePermission(companyId, resourceName, primKey, ActionKeys.VIEW, roleId);

		InOrder inOrder = inOrder(mockResourcePermission, mockResourcePermissionLocalService);
		inOrder.verify(mockResourcePermission, times(1)).setCompanyId(companyId);
		inOrder.verify(mockResourcePermission, times(1)).setName(resourceName);
		inOrder.verify(mockResourcePermission, times(1)).setScope(ResourceConstants.SCOPE_INDIVIDUAL);
		inOrder.verify(mockResourcePermission, times(1)).setPrimKey(primKey);
		inOrder.verify(mockResourcePermission, times(1)).setPrimKeyId(GetterUtil.getLong(primKey, 0));
		inOrder.verify(mockResourcePermission, times(1)).setRoleId(roleId);
		inOrder.verify(mockResourcePermission, times(1)).setActionIds(bitwiseValue);
		inOrder.verify(mockResourcePermission, times(1)).setViewActionId(true);
		inOrder.verify(mockResourcePermissionLocalService, times(1)).addResourcePermission(mockResourcePermission);
	}

	@Test
	public void addResourcePermission_WhenActionIdIsNotView_ThenCreatesAndConfiguresTheResourcePermissionWithoutViewActionIdTrue() throws Exception {
		long nextId = 123L;
		Long bitwiseValue = 456l;
		long companyId = 789L;
		long roleId = 22L;
		String resourceName = "myResourcePermissionName";
		String primKey = "654987";
		when(mockResourceActionLocalService.getResourceAction(resourceName, ActionKeys.UPDATE)).thenReturn(mockResourceAction);
		when(mockResourceAction.getBitwiseValue()).thenReturn(bitwiseValue);
		when(mockCounterLocalService.increment(ResourcePermission.class.getName(), 1)).thenReturn(nextId);
		when(mockResourcePermissionLocalService.createResourcePermission(nextId)).thenReturn(mockResourcePermission);

		rolePermissionUtil.addResourcePermission(companyId, resourceName, primKey, ActionKeys.UPDATE, roleId);

		InOrder inOrder = inOrder(mockResourcePermission, mockResourcePermissionLocalService);
		inOrder.verify(mockResourcePermission, times(1)).setCompanyId(companyId);
		inOrder.verify(mockResourcePermission, times(1)).setName(resourceName);
		inOrder.verify(mockResourcePermission, times(1)).setScope(ResourceConstants.SCOPE_INDIVIDUAL);
		inOrder.verify(mockResourcePermission, times(1)).setPrimKey(primKey);
		inOrder.verify(mockResourcePermission, times(1)).setPrimKeyId(GetterUtil.getLong(primKey, 0));
		inOrder.verify(mockResourcePermission, times(1)).setRoleId(roleId);
		inOrder.verify(mockResourcePermission, times(1)).setActionIds(bitwiseValue);
		inOrder.verify(mockResourcePermissionLocalService, times(1)).addResourcePermission(mockResourcePermission);
		verify(mockResourcePermission, never()).setViewActionId(anyBoolean());
	}

	@Test
	public void getIndividualResourcePermission_WhenException_ThenReturnsEmptyOptional() throws PortalException {
		long companyId = 789L;
		long roleId = 22L;
		String resourceName = "myResourcePermissionName";
		String primKey = "654987";
		when(mockResourcePermissionLocalService.getResourcePermission(companyId, resourceName, ResourceConstants.SCOPE_INDIVIDUAL, primKey, roleId)).thenThrow(new PortalException());

		Optional<ResourcePermission> result = rolePermissionUtil.getIndividualResourcePermission(companyId, resourceName, primKey, roleId);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getIndividualResourcePermission_WhenNoError_ThenReturnsOptionalWithTheResourcePermissionFound() throws PortalException {
		long companyId = 789L;
		long roleId = 22L;
		String resourceName = "myResourcePermissionName";
		String primKey = "654987";
		when(mockResourcePermissionLocalService.getResourcePermission(companyId, resourceName, ResourceConstants.SCOPE_INDIVIDUAL, primKey, roleId)).thenReturn(mockResourcePermission);

		Optional<ResourcePermission> result = rolePermissionUtil.getIndividualResourcePermission(companyId, resourceName, primKey, roleId);

		assertThat(result.get(), sameInstance(mockResourcePermission));
	}

	@Test(expected = RolePermissionException.class)
	public void removeActionIdFromResourcePermission_WhenException_ThenThrowsRolePermissionException() throws Exception {
		rolePermissionUtil.removeActionIdFromResourcePermission(ActionKeys.UPDATE, null);
	}

	@Test
	public void removeActionIdFromResourcePermission_WhenActionIdIsNotView_ThenConfiguresAndUpdatesTheResourceActionWithoutViewActionId() throws Exception {
		rolePermissionUtil.removeActionIdFromResourcePermission(ActionKeys.UPDATE, mockResourcePermission);

		InOrder inOrder = inOrder(mockResourcePermission, mockResourcePermissionLocalService);
		inOrder.verify(mockResourcePermission, times(1)).removeResourceAction(ActionKeys.UPDATE);
		inOrder.verify(mockResourcePermissionLocalService, times(1)).updateResourcePermission(mockResourcePermission);
		verify(mockResourcePermission, never()).setViewActionId(anyBoolean());
	}

	@Test
	public void removeActionIdFromResourcePermission_WhenActionIdIsView_ThenConfiguresAndUpdatesTheResourceActionWithViewActionIdFalse() throws Exception {
		rolePermissionUtil.removeActionIdFromResourcePermission(ActionKeys.VIEW, mockResourcePermission);

		InOrder inOrder = inOrder(mockResourcePermission, mockResourcePermissionLocalService);
		inOrder.verify(mockResourcePermission, times(1)).setViewActionId(false);
		inOrder.verify(mockResourcePermission, times(1)).removeResourceAction(ActionKeys.VIEW);
		inOrder.verify(mockResourcePermissionLocalService, times(1)).updateResourcePermission(mockResourcePermission);
	}
}
