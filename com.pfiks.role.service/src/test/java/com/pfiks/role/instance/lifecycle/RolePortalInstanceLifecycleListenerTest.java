/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.instance.lifecycle;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.liferay.portal.kernel.model.Company;
import com.pfiks.role.service.RoleCreatorService;

public class RolePortalInstanceLifecycleListenerTest {

	private static final long COMPANY_ID = 1;

	@Mock
	private Company mockCompany;

	@Mock
	private RoleCreatorService mockRoleCreatorService;

	private RolePortalInstanceLifecycleListener rolePortalInstanceLifecycleListener;

	@Test
	public void portalInstanceRegistered_WithCompany_CreatesRoleForCompany() throws Exception {

		rolePortalInstanceLifecycleListener.portalInstanceRegistered(mockCompany);

		verify(mockRoleCreatorService, times(1)).createRoles(mockCompany);

	}

	@Before
	public void setUp() throws Exception {

		initMocks(this);

		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);

		rolePortalInstanceLifecycleListener = new RolePortalInstanceLifecycleListener();
		rolePortalInstanceLifecycleListener.setRoleCreatorService(mockRoleCreatorService);

	}

}
