/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.service.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.pfiks.common.io.InputOuputUtil;
import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.internal.service.RoleConfiguratorUtil;
import com.pfiks.role.internal.service.RoleFileService;
import com.pfiks.role.model.RoleModel;

public class RoleCreatorInputStreamServiceTest {

	private static final long COMPANY_ID = 1L;

	private static final String ROLE_NAME = "Test role name";

	@Mock
	private Company mockCompany;

	@Mock
	private InputOuputUtil mockInputOutputUtil;

	@Mock
	private InputStream mockInputStream;

	@Mock
	private Role mockRole;

	@Mock
	private RoleConfiguratorUtil mockRoleConfiguratorUtil;

	@Mock
	private RoleFileService mockRoleFileService;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Mock
	private RoleModel mockRoleModel;

	private RoleCreatorInputStreamServiceImpl roleCreatorInputStreamServiceImpl;

	@Test
	public void configureMissingRoleFromInputStream_WithCompanyAndWhenCalledWithoutSkipParameterAndNoRoleExistWithTheGivenName_ThenCreatesAnewRole() throws Exception {
		mockRoleFromInputStream();
		mockRoleFound(false);

		roleCreatorInputStreamServiceImpl.configureMissingRoleFromInputStream(mockCompany, mockInputStream);

		verify(mockRoleConfiguratorUtil, times(1)).createRole(COMPANY_ID, mockRoleModel);
	}

	@Test
	public void configureMissingRoleFromInputStream_WithCompanyAndWhenCalledWithoutSkipParameterAndRoleAlreadyExistWithTheGivenName_ThenTheRoleIsNotCreated() throws Exception {
		mockRoleFromInputStream();
		mockRoleFound(true);

		roleCreatorInputStreamServiceImpl.configureMissingRoleFromInputStream(mockCompany, mockInputStream);

		verifyNoInteractions(mockRoleConfiguratorUtil);
	}

	@Test
	public void configureMissingRoleFromInputStream_WithCompanyAndWhenCalledWithoutSkipParameterAndRoleIsCreated_ThenConfiguresPermissionForTheRole() throws Exception {
		mockRoleFromInputStream();
		mockRoleFound(false);
		when(mockRoleConfiguratorUtil.createRole(COMPANY_ID, mockRoleModel)).thenReturn(mockRole);

		roleCreatorInputStreamServiceImpl.configureMissingRoleFromInputStream(mockCompany, mockInputStream);

		verify(mockRoleConfiguratorUtil, times(1)).configurePermissions(mockRole, mockRoleModel, false);
	}

	@Test(expected = RoleConfigurationException.class)
	public void configureMissingRoleFromInputStream_WithCompanyAndWhenCalledWithoutSkipParameterAndThrowsException_ThenRoleConfiguratorExceptionIsThrown() throws Exception {
		roleCreatorInputStreamServiceImpl.configureMissingRoleFromInputStream(mockCompany, null);
	}

	@Test
	public void configureMissingRoleFromInputStream_WithCompanyAndWhenCalledWithSkipParameterAndNoRoleExistWithTheGivenName_ThenCreatesAnewRole() throws Exception {
		mockRoleFromInputStream();
		mockRoleFound(false);

		roleCreatorInputStreamServiceImpl.configureMissingRoleFromInputStream(mockCompany, mockInputStream, false);

		verify(mockRoleConfiguratorUtil, times(1)).createRole(COMPANY_ID, mockRoleModel);
	}

	@Test
	public void configureMissingRoleFromInputStream_WithCompanyAndWhenCalledWithSkipParameterAndRoleAlreadyExistWithTheGivenName_ThenTheRoleIsNotCreated() throws Exception {
		mockRoleFromInputStream();
		mockRoleFound(true);

		roleCreatorInputStreamServiceImpl.configureMissingRoleFromInputStream(mockCompany, mockInputStream, false);

		verifyNoInteractions(mockRoleConfiguratorUtil);
	}

	@Test(expected = RoleConfigurationException.class)
	public void configureMissingRoleFromInputStream_WithCompanyAndWhenCalledWithSkipParameterAndThrowsException_ThenRoleConfiguratorExceptionIsThrown() throws Exception {
		roleCreatorInputStreamServiceImpl.configureMissingRoleFromInputStream(mockCompany, null, false);
	}

	@Test
	public void configureMissingRoleFromInputStream_WithCompanyAndWhenSkipParameterIsFalse_ThenConfiguresPermissionForTheRoleWithFalseSkipParameter() throws Exception {
		mockRoleFromInputStream();
		mockRoleFound(false);
		when(mockRoleConfiguratorUtil.createRole(COMPANY_ID, mockRoleModel)).thenReturn(mockRole);

		roleCreatorInputStreamServiceImpl.configureMissingRoleFromInputStream(mockCompany, mockInputStream, false);

		verify(mockRoleConfiguratorUtil, times(1)).configurePermissions(mockRole, mockRoleModel, false);
	}

	@Test
	public void configureMissingRoleFromInputStream_WithCompanyAndWhenSkipParameterIsTrue_ThenConfiguresPermissionForTheRoleWithTrueSkipParameter() throws Exception {
		mockRoleFromInputStream();
		mockRoleFound(false);
		when(mockRoleConfiguratorUtil.createRole(COMPANY_ID, mockRoleModel)).thenReturn(mockRole);

		roleCreatorInputStreamServiceImpl.configureMissingRoleFromInputStream(mockCompany, mockInputStream, true);

		verify(mockRoleConfiguratorUtil, times(1)).configurePermissions(mockRole, mockRoleModel, true);
	}

	@Before
	public void setUp() {
		initMocks(this);

		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);

		roleCreatorInputStreamServiceImpl = new RoleCreatorInputStreamServiceImpl();
		roleCreatorInputStreamServiceImpl.setRoleConfiguratorUtil(mockRoleConfiguratorUtil);
		roleCreatorInputStreamServiceImpl.setRoleFileService(mockRoleFileService);
		roleCreatorInputStreamServiceImpl.setRoleLocalService(mockRoleLocalService);
	}

	private void mockRoleFound(boolean roleFound) throws RoleConfigurationException {
		when(mockRoleModel.getName()).thenReturn(ROLE_NAME);
		if (roleFound) {
			when(mockRoleLocalService.fetchRole(COMPANY_ID, ROLE_NAME)).thenReturn(mockRole);
		} else {
			when(mockRoleLocalService.fetchRole(COMPANY_ID, ROLE_NAME)).thenReturn(null);
		}
	}

	private void mockRoleFromInputStream() throws RoleConfigurationException {
		when(mockRoleFileService.getRoleModelFromInputStream(mockInputStream)).thenReturn(mockRoleModel);
	}

}
