/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.liferay.portal.kernel.model.ResourceAction;
import com.liferay.portal.kernel.model.ResourcePermission;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.exception.RolePermissionException;
import com.pfiks.role.internal.service.RolePermissionUtil;

public class RolePermissionServiceTest {

	private RolePermissionServiceImpl rolePermissionsServiceImpl;

	private static final long COMPANY_ID = 1L;
	private static final long ROLE_ID = 2L;
	private static final String RESOURCE_NAME = "myResourcePermissionName";
	private static final int RESOURCE_SCOPE = 33;
	private static final String PRIM_KEY = "654987";
	private static final String ACTION_ID_ONE = "RESOURCE_ACTION_ID_ONE";
	private static final String ROLE_NAME = "myRoleName";
	private static final String RESOURCE_ACTION_ID_TWO = "RESOURCE_ACTION_ID_TWO";

	@Mock
	private ResourcePermissionLocalService mockResourcePermissionLocalService;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Mock
	private RolePermissionUtil mockRolePermissionUtil;

	@Mock
	private ResourceAction mockResourceAction;

	@Mock
	private ResourcePermission mockResourcePermission;

	@Mock
	private Role mockRole;

	@Before
	public void setUp() {
		initMocks(this);

		rolePermissionsServiceImpl = new RolePermissionServiceImpl();
		rolePermissionsServiceImpl.setResourcePermissionLocalService(mockResourcePermissionLocalService);
		rolePermissionsServiceImpl.setRoleLocalService(mockRoleLocalService);
		rolePermissionsServiceImpl.setRolePermissionUtil(mockRolePermissionUtil);
	}

	@Test(expected = RolePermissionException.class)
	public void addIndividualPermissionForRole_WhenExceptionAddingActionIdToExistingResourcePermission_ThenThrowsRolePermissionException() throws Exception {
		when(mockRolePermissionUtil.getIndividualResourcePermission(COMPANY_ID, RESOURCE_NAME, PRIM_KEY, ROLE_ID)).thenReturn(Optional.of(mockResourcePermission));
		when(mockResourcePermission.hasActionId(ACTION_ID_ONE)).thenReturn(false);
		doThrow(new RolePermissionException("msg", null)).when(mockRolePermissionUtil).addActionIdToResourcePermission(ACTION_ID_ONE, mockResourcePermission);

		rolePermissionsServiceImpl.addIndividualPermissionForRole(COMPANY_ID, ROLE_ID, RESOURCE_NAME, PRIM_KEY, ACTION_ID_ONE);
	}

	@Test(expected = RolePermissionException.class)
	public void addIndividualPermissionForRole_WhenExceptionAddingNewResourcePermission_ThenThrowsRolePermissionException() throws Exception {
		when(mockRolePermissionUtil.getIndividualResourcePermission(COMPANY_ID, RESOURCE_NAME, PRIM_KEY, ROLE_ID)).thenReturn(Optional.empty());
		doThrow(new RolePermissionException("msg", null)).when(mockRolePermissionUtil).addResourcePermission(COMPANY_ID, RESOURCE_NAME, PRIM_KEY, ACTION_ID_ONE, ROLE_ID);

		rolePermissionsServiceImpl.addIndividualPermissionForRole(COMPANY_ID, ROLE_ID, RESOURCE_NAME, PRIM_KEY, ACTION_ID_ONE);
	}

	@Test
	public void addIndividualPermissionForRole_WhenResourcePermissionIsFoundAndAlreadyHasActionId_ThenNoChangesAreMade() throws Exception {
		when(mockRolePermissionUtil.getIndividualResourcePermission(COMPANY_ID, RESOURCE_NAME, PRIM_KEY, ROLE_ID)).thenReturn(Optional.of(mockResourcePermission));
		when(mockResourcePermission.hasActionId(ACTION_ID_ONE)).thenReturn(true);

		rolePermissionsServiceImpl.addIndividualPermissionForRole(COMPANY_ID, ROLE_ID, RESOURCE_NAME, PRIM_KEY, ACTION_ID_ONE);

		verify(mockRolePermissionUtil, never()).addActionIdToResourcePermission(anyString(), any(ResourcePermission.class));
		verify(mockRolePermissionUtil, never()).addResourcePermission(anyLong(), anyString(), anyString(), anyString(), anyLong());
	}

	@Test
	public void addIndividualPermissionForRole_WhenResourcePermissionIsFoundAndDoesNotHaveActionId_ThenAddsTheActionIdToTheResourcePermission() throws Exception {
		when(mockRolePermissionUtil.getIndividualResourcePermission(COMPANY_ID, RESOURCE_NAME, PRIM_KEY, ROLE_ID)).thenReturn(Optional.of(mockResourcePermission));
		when(mockResourcePermission.hasActionId(ACTION_ID_ONE)).thenReturn(false);

		rolePermissionsServiceImpl.addIndividualPermissionForRole(COMPANY_ID, ROLE_ID, RESOURCE_NAME, PRIM_KEY, ACTION_ID_ONE);

		verify(mockRolePermissionUtil, times(1)).addActionIdToResourcePermission(ACTION_ID_ONE, mockResourcePermission);
	}

	@Test
	public void addIndividualPermissionForRole_WhenNoExistingResourcePermissionIsFound_ThenAddsAnewOne() throws Exception {
		when(mockRolePermissionUtil.getIndividualResourcePermission(COMPANY_ID, RESOURCE_NAME, PRIM_KEY, ROLE_ID)).thenReturn(Optional.empty());

		rolePermissionsServiceImpl.addIndividualPermissionForRole(COMPANY_ID, ROLE_ID, RESOURCE_NAME, PRIM_KEY, ACTION_ID_ONE);

		verify(mockRolePermissionUtil, times(1)).addResourcePermission(COMPANY_ID, RESOURCE_NAME, PRIM_KEY, ACTION_ID_ONE, ROLE_ID);
	}

	@Test(expected = RolePermissionException.class)
	public void addPermissionForRole_WhenException_ThrowsRolePermissionException() throws Exception {
		when(mockRoleLocalService.fetchRole(COMPANY_ID, ROLE_NAME)).thenReturn(null);

		rolePermissionsServiceImpl.addPermissionForRole(COMPANY_ID, ROLE_NAME, RESOURCE_NAME, 1, PRIM_KEY, ACTION_ID_ONE);
	}

	@Test
	public void addPermissionForRole_WhenNoErrors_ThenAddsTheActionIdToTheResourcePermissionForTheRole() throws Exception {
		when(mockRoleLocalService.fetchRole(COMPANY_ID, ROLE_NAME)).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(ROLE_ID);

		rolePermissionsServiceImpl.addPermissionForRole(COMPANY_ID, ROLE_NAME, RESOURCE_NAME, RESOURCE_SCOPE, PRIM_KEY, ACTION_ID_ONE);

		verify(mockResourcePermissionLocalService, times(1)).addResourcePermission(COMPANY_ID, RESOURCE_NAME, RESOURCE_SCOPE, PRIM_KEY, ROLE_ID, ACTION_ID_ONE);
	}

	@Test
	public void removeIndividualPermissionForRole_WhenResourcePermissionDoesNotExist_ThenNoActionIsPerformed() throws Exception {
		when(mockRolePermissionUtil.getIndividualResourcePermission(COMPANY_ID, RESOURCE_NAME, PRIM_KEY, ROLE_ID)).thenReturn(Optional.empty());

		rolePermissionsServiceImpl.removeIndividualPermissionForRole(COMPANY_ID, ROLE_ID, RESOURCE_NAME, PRIM_KEY, ACTION_ID_ONE);

		verify(mockRolePermissionUtil, never()).removeActionIdFromResourcePermission(any(), any());
	}

	@Test(expected = RolePermissionException.class)
	public void removeIndividualPermissionForRole_WhenExceptionRemovingPermission_ThenThrowsRolePermissionException() throws Exception {
		when(mockRolePermissionUtil.getIndividualResourcePermission(COMPANY_ID, RESOURCE_NAME, PRIM_KEY, ROLE_ID)).thenReturn(Optional.of(mockResourcePermission));
		when(mockResourcePermission.hasActionId(ACTION_ID_ONE)).thenReturn(true);
		doThrow(new RolePermissionException("msg", null)).when(mockRolePermissionUtil).removeActionIdFromResourcePermission(ACTION_ID_ONE, mockResourcePermission);

		rolePermissionsServiceImpl.removeIndividualPermissionForRole(COMPANY_ID, ROLE_ID, RESOURCE_NAME, PRIM_KEY, ACTION_ID_ONE);
	}

	@Test
	public void removeIndividualPermissionForRole_WhenResourcePermissionExistAndDoesNotHaveActionId_ThenNoActionIsPerformed() throws Exception {
		when(mockRolePermissionUtil.getIndividualResourcePermission(COMPANY_ID, RESOURCE_NAME, PRIM_KEY, ROLE_ID)).thenReturn(Optional.of(mockResourcePermission));
		when(mockResourcePermission.hasActionId(ACTION_ID_ONE)).thenReturn(false);

		rolePermissionsServiceImpl.removeIndividualPermissionForRole(COMPANY_ID, ROLE_ID, RESOURCE_NAME, PRIM_KEY, ACTION_ID_ONE);

		verify(mockRolePermissionUtil, never()).removeActionIdFromResourcePermission(any(), any());
	}

	@Test
	public void removeIndividualPermissionForRole_WhenResourcePermissionExistAndHasActionId_ThenRemovesActionIdFromResourcePermission() throws Exception {
		when(mockRolePermissionUtil.getIndividualResourcePermission(COMPANY_ID, RESOURCE_NAME, PRIM_KEY, ROLE_ID)).thenReturn(Optional.of(mockResourcePermission));
		when(mockResourcePermission.hasActionId(ACTION_ID_ONE)).thenReturn(true);

		rolePermissionsServiceImpl.removeIndividualPermissionForRole(COMPANY_ID, ROLE_ID, RESOURCE_NAME, PRIM_KEY, ACTION_ID_ONE);

		verify(mockRolePermissionUtil, times(1)).removeActionIdFromResourcePermission(ACTION_ID_ONE, mockResourcePermission);
	}

	@Test(expected = RoleConfigurationException.class)
	public void removePermissionFromRole_WhenException_ThenThrowsRoleConfiguratorException() throws Exception {
		when(mockRoleLocalService.fetchRole(COMPANY_ID, ROLE_NAME)).thenReturn(null);

		rolePermissionsServiceImpl.removePermissionFromRole(COMPANY_ID, ROLE_NAME, RESOURCE_NAME, PRIM_KEY, RESOURCE_SCOPE, ACTION_ID_ONE);
	}

	@Test
	public void removePermissionFromRole_WhenNoError_ThenRemovesPermissionForTheRole() throws Exception {
		when(mockRoleLocalService.fetchRole(COMPANY_ID, ROLE_NAME)).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(ROLE_ID);

		rolePermissionsServiceImpl.removePermissionFromRole(COMPANY_ID, ROLE_NAME, RESOURCE_NAME, PRIM_KEY, RESOURCE_SCOPE, ACTION_ID_ONE);

		verify(mockResourcePermissionLocalService, times(1)).removeResourcePermission(COMPANY_ID, RESOURCE_NAME, RESOURCE_SCOPE, PRIM_KEY, ROLE_ID, ACTION_ID_ONE);
	}

	@Test(expected = RoleConfigurationException.class)
	public void removePermissionsFromRole_WhenException_ThenThrowsRoleConfiguratorException() throws Exception {
		when(mockRoleLocalService.fetchRole(COMPANY_ID, ROLE_NAME)).thenReturn(null);

		rolePermissionsServiceImpl.removePermissionsFromRole(COMPANY_ID, ROLE_NAME, RESOURCE_NAME, PRIM_KEY, RESOURCE_SCOPE, new String[] { ACTION_ID_ONE, RESOURCE_ACTION_ID_TWO });
	}

	@Test
	public void removePermissionsFromRole_WhenNoError_ThenRemovesEachPermissionForTheRole() throws Exception {
		when(mockRoleLocalService.fetchRole(COMPANY_ID, ROLE_NAME)).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(ROLE_ID);

		rolePermissionsServiceImpl.removePermissionsFromRole(COMPANY_ID, ROLE_NAME, RESOURCE_NAME, PRIM_KEY, RESOURCE_SCOPE, new String[] { ACTION_ID_ONE, RESOURCE_ACTION_ID_TWO });

		verify(mockResourcePermissionLocalService, times(1)).removeResourcePermission(COMPANY_ID, RESOURCE_NAME, RESOURCE_SCOPE, PRIM_KEY, ROLE_ID, ACTION_ID_ONE);
		verify(mockResourcePermissionLocalService, times(1)).removeResourcePermission(COMPANY_ID, RESOURCE_NAME, RESOURCE_SCOPE, PRIM_KEY, ROLE_ID, RESOURCE_ACTION_ID_TWO);
	}

}
