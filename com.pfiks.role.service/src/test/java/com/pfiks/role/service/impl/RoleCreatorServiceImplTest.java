/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.service.impl;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.service.RoleCreator;

public class RoleCreatorServiceImplTest {

	@Mock
	private Company mockCompany1;

	@Mock
	private Company mockCompany2;

	@Mock
	private CompanyLocalService mockCompanyLocalService;

	@Mock
	private RoleCreator mockRoleCreator1;

	@Mock
	private RoleCreator mockRoleCreator2;

	private RoleCreatorServiceImpl roleCreatorServiceImpl;

	@Test
	public void createRoles_WhenNoRoleCreatorsSet_ThenDoesNotCreateAnyRole() throws RoleConfigurationException {
		roleCreatorServiceImpl.createRoles(mockCompany1);

		verifyNoInteractions(mockRoleCreator1);
		verifyNoInteractions(mockRoleCreator2);
	}

	@Test
	public void createRoles_WhenRoleCreatorsAreSet_ThenInvokesEachRoleCreatorForTheCompany() throws RoleConfigurationException {
		when(mockCompanyLocalService.getCompanies()).thenReturn(Collections.emptyList());
		roleCreatorServiceImpl.setRoleCreator(mockRoleCreator1);
		roleCreatorServiceImpl.setRoleCreator(mockRoleCreator2);

		roleCreatorServiceImpl.createRoles(mockCompany1);

		verify(mockRoleCreator1, times(1)).create(mockCompany1);
		verify(mockRoleCreator2, times(1)).create(mockCompany1);
	}

	@Test
	public void setRoleCreator_WhenRoleCreatorIsAdded_ThenInvokesTheRoleCreatorForAllTheCompanies() throws RoleConfigurationException {
		List<Company> companies = new ArrayList<>();
		companies.add(mockCompany1);
		companies.add(mockCompany2);
		when(mockCompanyLocalService.getCompanies()).thenReturn(companies);

		roleCreatorServiceImpl.setRoleCreator(mockRoleCreator1);

		verify(mockRoleCreator1, times(1)).create(mockCompany1);
		verify(mockRoleCreator1, times(1)).create(mockCompany2);
	}

	@Before
	public void setUp() {
		initMocks(this);

		roleCreatorServiceImpl = new RoleCreatorServiceImpl();
		roleCreatorServiceImpl.setCompanyLocalService(mockCompanyLocalService);
	}

	@Test
	public void unsetRoleCreator_WhenRoleCreatorIsUnset_ThenIsNoLongerInvokedForCompanies() throws RoleConfigurationException {
		when(mockCompanyLocalService.getCompanies()).thenReturn(Collections.emptyList());
		roleCreatorServiceImpl.setRoleCreator(mockRoleCreator1);
		roleCreatorServiceImpl.setRoleCreator(mockRoleCreator2);
		roleCreatorServiceImpl.createRoles(mockCompany1);
		verify(mockRoleCreator1, times(1)).create(mockCompany1);
		verify(mockRoleCreator2, times(1)).create(mockCompany1);

		roleCreatorServiceImpl.unsetRoleCreator(mockRoleCreator1);

		roleCreatorServiceImpl.createRoles(mockCompany1);
		verifyNoMoreInteractions(mockRoleCreator1);
	}
}
