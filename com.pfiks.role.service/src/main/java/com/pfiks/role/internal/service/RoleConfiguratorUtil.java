/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.internal.service;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.common.locale.LocaleMapUtil;
import com.pfiks.common.servicecontext.ServiceContextHelper;
import com.pfiks.role.model.ResourcePermissionModel;
import com.pfiks.role.model.RoleModel;

@Component(immediate = true, service = RoleConfiguratorUtil.class)
public class RoleConfiguratorUtil {

	private static final Log LOG = LogFactoryUtil.getLog(RoleConfiguratorUtil.class);

	private GroupLocalService groupLocalService;

	private LocaleMapUtil localeMapUtil;

	private ResourcePermissionLocalService resourcePermissionLocalService;

	private RoleLocalService roleLocalService;

	private ServiceContextHelper serviceContextHelper;

	private UserLocalService userLocalService;

	public void configurePermissions(Role role, RoleModel roleModel, boolean skipMissingResource) throws PortalException {
		List<ResourcePermissionModel> resourcePermissions = roleModel.getResourcePermissions();
		if (Validator.isNotNull(resourcePermissions) && !resourcePermissions.isEmpty()) {
			long companyId = role.getCompanyId();
			long roleId = role.getRoleId();
			for (ResourcePermissionModel resourcePermissionModel : resourcePermissions) {
				processRolePermission(skipMissingResource, companyId, roleId, resourcePermissionModel);
			}
		}
	}

	public Role createRole(long companyId, RoleModel roleModel) throws PortalException {
		String name = roleModel.getName();
		String title = roleModel.getTitle();
		final Map<Locale, String> titleMap = localeMapUtil.getLocaleMapForAvailableLocales(title);
		long companyGroupId = groupLocalService.getCompanyGroup(companyId).getGroupId();
		long userId = userLocalService.getGuestUserId(companyId);
		long classPK = 0;
		String subtype = "";
		ServiceContext serviceContext = serviceContextHelper.getServiceContext(companyId, companyGroupId, userId);
		Role role = roleLocalService.addRole(userId, Role.class.getName(), classPK, name, titleMap, titleMap, roleModel.getType(), subtype, serviceContext);
		LOG.debug("Created new role with name: " + name + ", roleId: " + role.getRoleId());
		return role;
	}

	@Reference
	protected void setGroupLocalService(GroupLocalService groupLocalService) {
		this.groupLocalService = groupLocalService;
	}

	@Reference
	protected void setLocaleMapUtil(LocaleMapUtil localeMapUtil) {
		this.localeMapUtil = localeMapUtil;
	}

	@Reference
	protected void setResourcePermissionLocalService(ResourcePermissionLocalService resourcePermissionLocalService) {
		this.resourcePermissionLocalService = resourcePermissionLocalService;
	}

	@Reference
	protected void setRoleLocalService(RoleLocalService roleLocalService) {
		this.roleLocalService = roleLocalService;
	}

	@Reference
	protected void setServiceContextHelper(ServiceContextHelper serviceContextHelper) {
		this.serviceContextHelper = serviceContextHelper;
	}

	@Reference
	protected void setUserLocalService(UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	private void configureRoleResourcePermission(long companyId, long roleId, ResourcePermissionModel resourcePermissionModel) throws PortalException {
		List<String> actionIds = resourcePermissionModel.getActionIds();
		String primKeyValue = resourcePermissionModel.isPrimKeyCompany() ? String.valueOf(companyId) : resourcePermissionModel.getPrimKey();
		resourcePermissionLocalService.setResourcePermissions(companyId, resourcePermissionModel.getName(), resourcePermissionModel.getScope(), primKeyValue, roleId,
				actionIds.toArray(new String[actionIds.size()]));
	}

	private void processRolePermission(boolean skipMissingResource, long companyId, long roleId, ResourcePermissionModel resourcePermissionModel) throws PortalException {
		if (skipMissingResource) {
			try {
				configureRoleResourcePermission(companyId, roleId, resourcePermissionModel);
			} catch (PortalException e) {
				LOG.warn(e);
			}
		} else {
			configureRoleResourcePermission(companyId, roleId, resourcePermissionModel);
		}
	}

}
