/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.internal.service;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.ResourceAction;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.ResourcePermission;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ResourceActionLocalService;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;
import com.liferay.portal.kernel.util.GetterUtil;
import com.pfiks.role.exception.RolePermissionException;

@Component(immediate = true, service = RolePermissionUtil.class)
public class RolePermissionUtil {

	private CounterLocalService counterLocalService;
	private ResourceActionLocalService resourceActionLocalService;
	private ResourcePermissionLocalService resourcePermissionLocalService;

	public void addActionIdToResourcePermission(String actionIdToAdd, ResourcePermission resourcePermission) throws RolePermissionException {
		try {
			if (ActionKeys.VIEW.equals(actionIdToAdd)) {
				resourcePermission.setViewActionId(true);
			}
			resourcePermission.addResourceAction(actionIdToAdd);
			resourcePermissionLocalService.updateResourcePermission(resourcePermission);
		} catch (Exception e) {
			throw new RolePermissionException("Exception adding permission to role", e);
		}
	}

	public void addResourcePermission(long companyId, String resourceName, String primaryKey, String actionIdToAdd, long roleId) throws RolePermissionException {
		try {
			ResourceAction resourceAction = resourceActionLocalService.getResourceAction(resourceName, actionIdToAdd);
			ResourcePermission resourcePermission = resourcePermissionLocalService.createResourcePermission(counterLocalService.increment(ResourcePermission.class.getName(), 1));
			resourcePermission.setCompanyId(companyId);
			resourcePermission.setName(resourceName);
			resourcePermission.setScope(ResourceConstants.SCOPE_INDIVIDUAL);
			resourcePermission.setPrimKey(primaryKey);
			resourcePermission.setPrimKeyId(GetterUtil.getLong(primaryKey, 0));
			resourcePermission.setRoleId(roleId);
			resourcePermission.setActionIds(resourceAction.getBitwiseValue());
			if (ActionKeys.VIEW.equals(actionIdToAdd)) {
				resourcePermission.setViewActionId(true);
			}
			resourcePermissionLocalService.addResourcePermission(resourcePermission);
		} catch (Exception e) {
			throw new RolePermissionException("Exception adding permission to role", e);
		}
	}

	public Optional<ResourcePermission> getIndividualResourcePermission(long companyId, String resourceName, String primaryKey, long roleId) {
		try {
			return Optional.ofNullable(resourcePermissionLocalService.getResourcePermission(companyId, resourceName, ResourceConstants.SCOPE_INDIVIDUAL, primaryKey, roleId));
		} catch (PortalException e) {
			return Optional.empty();
		}
	}

	public void removeActionIdFromResourcePermission(String actionIdToRemove, ResourcePermission resourcePermission) throws RolePermissionException {
		try {
			if (ActionKeys.VIEW.equals(actionIdToRemove)) {
				resourcePermission.setViewActionId(false);
			}
			resourcePermission.removeResourceAction(actionIdToRemove);
			resourcePermissionLocalService.updateResourcePermission(resourcePermission);
		} catch (Exception e) {
			throw new RolePermissionException("Exception Removing permission from role", e);
		}
	}

	@Reference
	protected void setCounterLocalService(CounterLocalService counterLocalService) {
		this.counterLocalService = counterLocalService;
	}

	@Reference
	protected void setResourceActionLocalService(ResourceActionLocalService resourceActionLocalService) {
		this.resourceActionLocalService = resourceActionLocalService;
	}

	@Reference
	protected void setResourcePermissionLocalService(ResourcePermissionLocalService resourcePermissionLocalService) {
		this.resourcePermissionLocalService = resourcePermissionLocalService;
	}

}
