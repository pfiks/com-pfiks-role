/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.internal.service;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.util.StreamUtil;
import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.model.RoleModel;

@Component(immediate = true, properties = {}, service = RoleFileService.class)
public class RoleFileService {

	public RoleModel getRoleModelFromInputStream(InputStream inputStream) throws RoleConfigurationException {

		try {

			JAXBContext jaxbContext = JAXBContext.newInstance(RoleModel.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			return (RoleModel) unmarshaller.unmarshal(inputStream);

		} catch (Exception e) {

			throw new RoleConfigurationException("Exception loading role from input stream", e);

		} finally {

			StreamUtil.cleanUp(true, inputStream);

		}

	}

}
