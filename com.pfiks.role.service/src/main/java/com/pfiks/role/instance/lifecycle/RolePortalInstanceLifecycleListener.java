/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.instance.lifecycle;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.instance.lifecycle.BasePortalInstanceLifecycleListener;
import com.liferay.portal.instance.lifecycle.PortalInstanceLifecycleListener;
import com.liferay.portal.kernel.model.Company;
import com.pfiks.role.service.RoleCreatorService;

@Component(immediate = true, service = PortalInstanceLifecycleListener.class)
public class RolePortalInstanceLifecycleListener extends BasePortalInstanceLifecycleListener {

	private RoleCreatorService roleCreatorService;

	@Override
	public void portalInstanceRegistered(Company company) throws Exception {
		roleCreatorService.createRoles(company);
	}

	@Reference
	protected void setRoleCreatorService(RoleCreatorService roleCreatorService) {
		this.roleCreatorService = roleCreatorService;
	}

}
