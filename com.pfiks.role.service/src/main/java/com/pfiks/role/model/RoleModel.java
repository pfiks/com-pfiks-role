/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.model;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.liferay.portal.kernel.util.Validator;

/**
 * The RoleModel class.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "name", "title", "type", "resourcePermissions" })
@XmlRootElement(name = "role")
public class RoleModel {

	@XmlElement(name = "name", required = true)
	protected String name;

	@XmlElement(name = "title", required = false)
	protected String title;

	@XmlElement(name = "type", required = true)
	protected int type;

	@XmlElement(name = "resourcePermission", required = false)
	protected List<ResourcePermissionModel> resourcePermissions;

	/**
	 * Gets the value of the name property.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the value of the title property.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return Validator.isNull(title) ? name : title;
	}

	/**
	 * Gets the value of the type property.
	 * 
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * Gets the resource permissions.
	 *
	 * @return the resource permissions
	 */
	public List<ResourcePermissionModel> getResourcePermissions() {
		if (resourcePermissions == null) {
			resourcePermissions = new LinkedList<>();
		}
		return resourcePermissions;
	}

}
