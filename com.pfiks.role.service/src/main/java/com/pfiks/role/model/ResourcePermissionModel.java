/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.model;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resourcePermission", propOrder = { "name", "scope", "primKey", "primKeyCompany", "actionIds" })
public class ResourcePermissionModel {

	@XmlElement(name = "name", required = true)
	protected String name;

	@XmlElement(name = "scope", required = true)
	protected int scope;

	@XmlElement(name = "primKey", required = true)
	protected String primKey;

	@XmlElement(name = "primKeyCompany", type = Boolean.class, required = true)
	protected Boolean primKeyCompany;

	@XmlElement(name = "actionId", required = true)
	protected List<String> actionIds;

	public String getName() {
		return name;
	}

	public int getScope() {
		return scope;
	}

	public String getPrimKey() {
		return primKey;
	}

	public Boolean isPrimKeyCompany() {
		return primKeyCompany;
	}

	public List<String> getActionIds() {
		if (actionIds == null) {
			actionIds = new LinkedList<>();
		}
		return actionIds;
	}

}
