/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.service.RoleCreator;
import com.pfiks.role.service.RoleCreatorService;

@Component(immediate = true, service = RoleCreatorService.class)
public class RoleCreatorServiceImpl implements RoleCreatorService {

	private CompanyLocalService companyLocalService;

	private List<RoleCreator> roleCreators = new ArrayList<>();

	@Override
	public void createRoles(Company company) throws RoleConfigurationException {

		for (RoleCreator roleCreator : roleCreators) {
			roleCreator.create(company);
		}

	}

	private void createRoleForAllCompanies(RoleCreator roleCreator) throws RoleConfigurationException {

		List<Company> companies = companyLocalService.getCompanies();

		for (Company company : companies) {
			roleCreator.create(company);
		}

	}

	@Reference
	protected void setCompanyLocalService(CompanyLocalService companyLocalService) {
		this.companyLocalService = companyLocalService;
	}

	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void setRoleCreator(RoleCreator roleCreator) throws RoleConfigurationException {
		roleCreators.add(roleCreator);
		createRoleForAllCompanies(roleCreator);
	}

	protected void unsetRoleCreator(RoleCreator roleCreator) {
		roleCreators.remove(roleCreator);
	}

}
