/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.service.impl;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.internal.service.RoleConfiguratorUtil;
import com.pfiks.role.internal.service.RoleFileService;
import com.pfiks.role.model.RoleModel;
import com.pfiks.role.service.RoleCreatorInputStreamService;

@Component(immediate = true, service = RoleCreatorInputStreamService.class)
public class RoleCreatorInputStreamServiceImpl implements RoleCreatorInputStreamService {

	private static final Log LOG = LogFactoryUtil.getLog(RoleCreatorInputStreamServiceImpl.class);

	private RoleConfiguratorUtil roleConfiguratorUtil;

	private RoleFileService roleFileService;

	private RoleLocalService roleLocalService;

	@Override
	public void configureMissingRoleFromInputStream(Company company, InputStream roleConfigurationInputStream) throws RoleConfigurationException {

		try {

			RoleModel roleModel = roleFileService.getRoleModelFromInputStream(roleConfigurationInputStream);
			createMissingRole(company.getCompanyId(), roleModel, false);

		} catch (Exception e) {
			throw new RoleConfigurationException("Exception configuring missing role", e);
		}

	}

	@Override
	public void configureMissingRoleFromInputStream(Company company, InputStream roleConfigurationInputStream, boolean skipMissingResource) throws RoleConfigurationException {

		try {

			RoleModel roleModel = roleFileService.getRoleModelFromInputStream(roleConfigurationInputStream);
			createMissingRole(company.getCompanyId(), roleModel, skipMissingResource);

		} catch (Exception e) {
			throw new RoleConfigurationException("Exception configuring missing role", e);
		}

	}

	private void createMissingRole(long companyId, RoleModel roleModel, boolean skipMissingResource) throws PortalException {

		String roleName = roleModel.getName();

		if (Validator.isNull(roleLocalService.fetchRole(companyId, roleName))) {
			Role role = roleConfiguratorUtil.createRole(companyId, roleModel);
			roleConfiguratorUtil.configurePermissions(role, roleModel, skipMissingResource);
		} else {
			LOG.debug("Role already exists with name: " + roleName + " in companyId: " + companyId);
		}

	}

	@Reference
	protected void setRoleConfiguratorUtil(RoleConfiguratorUtil roleConfiguratorUtil) {
		this.roleConfiguratorUtil = roleConfiguratorUtil;
	}

	@Reference
	protected void setRoleFileService(RoleFileService roleFileService) {
		this.roleFileService = roleFileService;
	}

	@Reference
	protected void setRoleLocalService(RoleLocalService roleLocalService) {
		this.roleLocalService = roleLocalService;
	}

}
