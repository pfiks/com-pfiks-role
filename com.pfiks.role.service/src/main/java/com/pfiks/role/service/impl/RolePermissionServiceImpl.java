/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.service.impl;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.ResourcePermission;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.exception.RolePermissionException;
import com.pfiks.role.internal.service.RolePermissionUtil;
import com.pfiks.role.service.RolePermissionService;

@Component(immediate = true, service = RolePermissionService.class)
public class RolePermissionServiceImpl implements RolePermissionService {

	private static final Log LOG = LogFactoryUtil.getLog(RolePermissionServiceImpl.class);

	private ResourcePermissionLocalService resourcePermissionLocalService;
	private RoleLocalService roleLocalService;
	private RolePermissionUtil rolePermissionUtil;

	@Override
	public void addIndividualPermissionForRole(long companyId, long roleIdToUpdate, String resourceName, String primaryKey, String actionIdToAdd) throws RolePermissionException {
		LOG.debug("Adding role permission for companyId: " + companyId + ", roleId: " + roleIdToUpdate + ", primaryKey: " + primaryKey);

		Optional<ResourcePermission> existingResourcePermission = rolePermissionUtil.getIndividualResourcePermission(companyId, resourceName, primaryKey, roleIdToUpdate);

		if (existingResourcePermission.isPresent()) {
			ResourcePermission resourcePermission = existingResourcePermission.get();
			if (!resourcePermission.hasActionId(actionIdToAdd)) {
				rolePermissionUtil.addActionIdToResourcePermission(actionIdToAdd, resourcePermission);
			}

		} else {
			rolePermissionUtil.addResourcePermission(companyId, resourceName, primaryKey, actionIdToAdd, roleIdToUpdate);
		}
	}

	@Override
	public void addPermissionForRole(long companyId, String roleName, String resourceName, int scope, String primKeyValue, String actionIdToAdd) throws RolePermissionException {
		try {
			LOG.debug("Adding role permission for companyId: " + companyId + ", roleName: " + roleName);

			Role roleToUpdate = roleLocalService.fetchRole(companyId, roleName);
			resourcePermissionLocalService.addResourcePermission(companyId, resourceName, scope, primKeyValue, roleToUpdate.getRoleId(), actionIdToAdd);

		} catch (Exception e) {
			throw new RolePermissionException("Exception adding permission to role", e);
		}
	}

	@Override
	public void removeIndividualPermissionForRole(long companyId, long roleIdToUpdate, String resourceName, String primaryKey, String actionIdToRemove) throws RolePermissionException {
		LOG.debug("Removing role permission for companyId: " + companyId + ", roleId: " + roleIdToUpdate + ", primaryKey: " + primaryKey);
		Optional<ResourcePermission> existingResourcePermission = rolePermissionUtil.getIndividualResourcePermission(companyId, resourceName, primaryKey, roleIdToUpdate);
		if (existingResourcePermission.isPresent()) {
			ResourcePermission resourcePermission = existingResourcePermission.get();
			if (resourcePermission.hasActionId(actionIdToRemove)) {
				rolePermissionUtil.removeActionIdFromResourcePermission(actionIdToRemove, resourcePermission);
			}
		}

	}

	@Override
	public void removePermissionFromRole(long companyId, String roleName, String className, String primaryKey, int resourceScope, String actionIdToRemove) throws RoleConfigurationException {

		try {

			LOG.debug("Removing role permission for companyId: " + companyId + ", roleName: " + roleName);
			Role roleToUpdate = roleLocalService.fetchRole(companyId, roleName);
			resourcePermissionLocalService.removeResourcePermission(companyId, className, resourceScope, primaryKey, roleToUpdate.getRoleId(), actionIdToRemove);

		} catch (Exception e) {
			throw new RoleConfigurationException("Exception removing permission from role", e);
		}

	}

	@Override
	public void removePermissionsFromRole(long companyId, String roleName, String className, String primaryKey, int resourceScope, String[] actionIdsToRemove) throws RoleConfigurationException {

		try {

			LOG.debug("Removing role permission for companyId: " + companyId + ", roleName: " + roleName);
			long roleId = roleLocalService.fetchRole(companyId, roleName).getRoleId();

			for (String actionIdToRemove : actionIdsToRemove) {
				resourcePermissionLocalService.removeResourcePermission(companyId, className, resourceScope, primaryKey, roleId, actionIdToRemove);
			}

		} catch (Exception e) {
			throw new RoleConfigurationException("Exception removing permissions from role", e);
		}

	}

	@Reference
	protected void setResourcePermissionLocalService(ResourcePermissionLocalService resourcePermissionLocalService) {
		this.resourcePermissionLocalService = resourcePermissionLocalService;
	}

	@Reference
	protected void setRoleLocalService(RoleLocalService roleLocalService) {
		this.roleLocalService = roleLocalService;
	}

	@Reference
	protected void setRolePermissionUtil(RolePermissionUtil rolePermissionUtil) {
		this.rolePermissionUtil = rolePermissionUtil;
	}

}
