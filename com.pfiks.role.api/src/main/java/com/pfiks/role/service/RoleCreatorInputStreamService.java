/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.service;

import java.io.InputStream;

import com.liferay.portal.kernel.model.Company;
import com.pfiks.role.exception.RoleConfigurationException;

/**
 * The Interface RoleCreatorInputStreamService.
 */
public interface RoleCreatorInputStreamService {

	/**
	 * Configures role from input stream in role does not exist
	 *
	 * @param company the company to create the role in
	 * @param roleConfigurationInputStream the role configuration input stream
	 * @throws RoleConfigurationException the role configuration exception
	 */
	public void configureMissingRoleFromInputStream(Company company, InputStream roleConfigurationInputStream) throws RoleConfigurationException;

	/**
	 * Configures role from input stream in role does not exist
	 *
	 * @param company the company to create the role in
	 * @param roleConfigurationInputStream the role configuration input stream
	 * @param skipMissingResource the skip missing resource
	 * @throws RoleConfigurationException the role configuration exception
	 */
	public void configureMissingRoleFromInputStream(Company company, InputStream roleConfigurationInputStream, boolean skipMissingResource) throws RoleConfigurationException;

}
