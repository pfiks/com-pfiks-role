/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.service;

import com.pfiks.role.exception.RoleConfigurationException;
import com.pfiks.role.exception.RolePermissionException;

/**
 * The Interface RolePermissionService.
 */
public interface RolePermissionService {

	/**
	 * Adds the permission to the role.
	 *
	 * @param companyId the companyId
	 * @param roleIdToUpdate the roleId to update
	 * @param resourceName the resource name
	 * @param primaryKey the resource primKey
	 * @param actionIdToAdd the actionId to add
	 * @throws RolePermissionException if any exception occurs while adding the
	 *             permission
	 */
	public void addIndividualPermissionForRole(long companyId, long roleIdToUpdate, String resourceName, String primaryKey, String actionIdToAdd) throws RolePermissionException;

	/**
	 * Adds the permission to the role.
	 *
	 * @param companyId the companyId
	 * @param roleName the roleName to update
	 * @param resourceName the resource name
	 * @param scope the resource scope
	 * @param primKeyValue the resource primKey
	 * @param actionIdToAdd the actionId to add
	 * @throws RolePermissionException if any exception occurs while adding the
	 *             permission
	 */
	public void addPermissionForRole(long companyId, String roleName, String resourceName, int scope, String primKeyValue, String actionIdToAdd) throws RolePermissionException;

	/**
	 * Removes the permission from the role.
	 *
	 * @param companyId the companyId
	 * @param roleIdToUpdate the roleId to update
	 * @param resourceName the resource name
	 * @param primaryKey the resource primKey
	 * @param actionIdToRemove the actionId to remove
	 * @throws RolePermissionException if any exception occurs while removing
	 *             the permission
	 */
	public void removeIndividualPermissionForRole(long companyId, long roleIdToUpdate, String resourceName, String primaryKey, String actionIdToRemove) throws RolePermissionException;

	/**
	 * Removes the permission from role.
	 *
	 * @param companyId the company id
	 * @param roleName the role name
	 * @param className the class name
	 * @param primaryKey the primary key
	 * @param resourceScope the resource scope
	 * @param actionIdToRemove the action id to remove
	 * @throws RoleConfigurationException the role configurator exception
	 */
	public void removePermissionFromRole(long companyId, String roleName, String className, String primaryKey, int resourceScope, String actionIdToRemove) throws RoleConfigurationException;

	/**
	 * Removes the permissions from role.
	 *
	 * @param companyId the company id
	 * @param roleName the role name
	 * @param className the class name
	 * @param primaryKey the primary key
	 * @param resourceScope the resource scope
	 * @param actionIdsToRemove the action ids to remove
	 * @throws RoleConfigurationException the role configurator exception
	 */
	public void removePermissionsFromRole(long companyId, String roleName, String className, String primaryKey, int resourceScope, String[] actionIdsToRemove) throws RoleConfigurationException;

}
