/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.service;

import com.liferay.portal.kernel.model.Company;
import com.pfiks.role.exception.RoleConfigurationException;

/**
 * The Interface RoleCreatorService.
 */
public interface RoleCreatorService {

	/**
	 * Creates roles for a given company
	 *
	 * @param company the company to create roles in
	 * @throws RoleConfigurationException the role configuration exception
	 */
	public void createRoles(Company company) throws RoleConfigurationException;

}
