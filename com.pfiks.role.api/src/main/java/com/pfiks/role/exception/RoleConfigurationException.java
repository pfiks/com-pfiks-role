/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.role.exception;

/**
 * Exception used when updating and creating roles
 */
@SuppressWarnings("serial")
public class RoleConfigurationException extends Exception {

	/**
	 * Initialises a new RoleConfigurationException
	 *
	 * @param message the exception message
	 */
	public RoleConfigurationException(String message) {
		super(message);
	}

	/**
	 * Initialises a new RoleConfigurationException
	 *
	 * @param message the exception message
	 * @param exception the exception
	 */
	public RoleConfigurationException(String message, Throwable exception) {
		super(message, exception);
	}

}
